from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests
from pprint import pprint

def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geo_url, params=params)
    content = json.loads(response.content)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(weather_url, params=params)
    content = json.loads(response.content)
    weather = {"weather_temp": content["main"]["temp"],
               "description": content["weather"][0]["description"],
                }
    return weather



def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}"
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
